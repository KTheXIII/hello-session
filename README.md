# Hello Session with Expressjs

Learning to use session with Expressjs

## Requirements
  - [Nodejs](https://nodejs.org/en/)

## How To Use

Clone this repo


### Install packges

With `npm`
```shell
npm i
```

With `yarn`
```shell
yarn
```

### Run in development

With `npm`
```shell
npm run dev
```

With `yarn`
```shell
yarn run dev
```