const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const consola = require('consola')

const {
  S_NAME,
  S_LIFETIME,
  S_SECRET,
  NODE_ENV,
} = require('../configs/env.config')
const IN_PROD = false //NODE_ENV == 'production'

// TODO: DB
const users = [
  { id: 1, name: 'Charlie', email: 'charlie@gmail.com', password: 'secret' },
  { id: 2, name: 'Felix', email: 'felix@gmail.com', password: 'secret' },
  {
    id: 3,
    name: 'Pratchaya',
    email: 'pratchaya@gmail.com',
    password: 'secret',
  },
]

const app = express()

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use(
  session({
    name: S_NAME,
    resave: false,
    saveUninitialized: false,
    secret: S_SECRET,
    cookie: {
      maxAge: S_LIFETIME,
      sameSite: true,
      secure: IN_PROD,
    },
  })
)

// Middleware
function redirectLogin(req, res, next) {
  if (!req.session.userID) {
    res.redirect('/login')
  } else {
    next()
  }
}

function redirectHome(req, res, next) {
  if (req.session.userID) {
    res.redirect('/home')
  } else {
    next()
  }
}

app.use((req, res, next) => {
  const { userID } = req.session
  if (userID) {
    res.locals.user = users.find((user) => user.id == userID)
  }
  next()
})

app.get('/', (req, res) => {
  const { userID } = req.session

  res.send(`
    <h1>Welcome!</h1>
    ${
      userID
        ? `
    <a href='/home'>Home</a>
    <form method='post' action='/logout'>
      <button>Logout</button>
    </form>
    `
        : `
    <a href='/login'>Login</a>
    <a href='/register'>Register</a>
    `
    }`)
})
app.get('/home', redirectLogin, (req, res) => {
  const { user } = res.locals
  res.send(`
  <h1>Home</h1>
  <a href='/'>Main</a>
  <ul>
    <li>Name: ${user.name}</li>
    <li>Email: ${user.email}</li>
  </ul>
  `)
})

app.get('/login', redirectHome, (req, res) => {
  res.send(`
    <h1>Login</h1>
    <form method='post' action='/login'> 
      <input type='email' name='email' placeholder='Email' required />
      <input type='password' name='password' placeholder='Password' required />
      <input type='submit' />
    </form>
    <a href='/register'>Register</a>
  `)
})
app.get('/register', (req, res) => {
  res.send(`
    <h1>Login</h1>
    <form method='post' action='/register'>
      <input type='text' name='name' placeholder='Name' required />
      <input type='email' name='email' placeholder='Email' required />
      <input type='password' name='password' placeholder='Password' required />
      <input type='submit' />
    </form>
    <a href='/Login'>Login</a>
  `)
})

app.post('/login', redirectHome, (req, res) => {
  const { email, password } = req.body

  if (email && password) {
    // TODO: Validation
    const user = users.find(
      (user) => user.email == email && user.password == password // TODO: hash
    )

    if (user) {
      req.session.userID = user.id
      return res.redirect('/home')
    }
  }

  res.redirect('/login')
})
app.post('/register', redirectHome, (req, res) => {
  const { name, email, password } = req.body

  if (name && email && password) {
    // TODO: Validation
    const exists = users.some((user) => user.email == email)
    if (!exists) {
      const user = {
        id: users.length + 1,
        name,
        email,
        password, // TODO: hash
      }

      users.push(user)

      req.session.userID = user.id

      return res.redirect('/home')
    }
  }

  res.redirect('/register') // TODO: query string errors
})

app.post('/logout', redirectLogin, (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return res.redirect('/home')
    }

    res.clearCookie(S_NAME)
    res.redirect('/login')
  })
})

module.exports = app
