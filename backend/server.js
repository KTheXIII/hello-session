const http = require('http')
const consola = require('consola')
const app = require('./app')

const { PORT } = require('../configs/env.config')

const server = http.createServer(app)

server.listen(PORT, () => {
  consola.ready({
    message: `Server listening on http://localhost:${PORT}`,
    badge: true,
  })
})
