const TWO_HOURS = 1000 * 60 * 60 * 2

const PORT = process.env.PORT || 5000
const S_NAME = process.env.S_NAME || 'sid'
const S_SECRET = process.env.S_SECRET || 'hello there'
const S_LIFETIME = process.env.S_LIFETIME || TWO_HOURS
const NODE_ENV = 'development'

module.exports = {
  PORT,
  S_NAME,
  S_LIFETIME,
  S_SECRET,
  NODE_ENV,
}
